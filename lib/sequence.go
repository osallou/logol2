package logol

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	lru "github.com/hashicorp/golang-lru"
)

// Sequence describes a sequence to analyze
type Sequence struct {
	Path    string
	Size    int
	Content string
}

// SequenceLru is a LRU cache for sequence parts
type SequenceLru struct {
	Lru      *lru.Cache
	Sequence Sequence
}

// NewSequence initialize a Sequence
func NewSequence(path string) (seq Sequence) {
	seq = Sequence{}
	seq.Path = path
	file, err := os.Open(path)
	if err != nil {
		logger.Errorf("%s: %s", "failed to open sequence", err)
		panic(fmt.Sprintf("%s: %s", "failed to open sequence", err))
	}
	defer file.Close()
	stat, _ := file.Stat()
	seq.Size = int(stat.Size())
	return seq
}

// NewSequenceLru initialize a LRU cache for sequence
func NewSequenceLru(sequence Sequence) (seq SequenceLru) {
	logger.Debugf("Initialize sequence LRU")
	seq = SequenceLru{}
	seq.Sequence = sequence
	seq.Lru, _ = lru.New(10)
	return seq
}

// GetContent get content from sequence using LRU cache
func (s SequenceLru) GetContent(start int, end int) (content string) {
	logger.Debugf("Search in sequence %d:%d", start, end)
	keys := s.Lru.Keys()
	sRange := ""
	sStart := 0
	sEnd := 0
	for _, key := range keys {
		//log.Printf("Cache content: %s", key.(string))
		r := strings.Split(key.(string), ".")
		sStart, _ = strconv.Atoi(r[0])
		sEnd, _ = strconv.Atoi(r[1])
		if start >= sStart && end <= sEnd {
			sRange = key.(string)
			break
		}
	}
	logger.Debugf("seq start: %d, end: %d", sStart, sEnd)
	if sRange != "" {
		//log.Printf("Load sequence from cache")
		cache, _ := s.Lru.Get(sRange)
		if cache != nil {
			seqPart := cache.(string)
			logger.Debugf("cache, %d, %d", start, end)
			content = seqPart[start-sStart : end-sStart]
			return content
		}
	}
	file, _ := os.Open(s.Sequence.Path)
	defer file.Close()
	if end > s.Sequence.Size {
		end = s.Sequence.Size - 1
	}
	bufferSize := 10000
	if start+bufferSize > s.Sequence.Size {
		bufferSize = end - start
	}
	if end-start > bufferSize {
		bufferSize = end - start
	}
	logger.Debugf("Load from sequence %d, %d", start, end)
	if bufferSize < 0 {
		return ""
	}
	buffer := make([]byte, bufferSize)
	file.ReadAt(buffer, int64(start))
	// get content
	content = string(buffer)
	key := fmt.Sprintf("%d.%d", start, end)
	//logger.Debugf("Save in LRU %s", key)
	s.Lru.Add(key, content)
	return content

}

// Morphisms and sequence type
var morphism map[string]map[string]string

func initMorphisms(kind string) {
	complement := make(map[string]string)
	if kind == "" || kind == "dna" || kind == "rna" {
		complement["a"] = "t"
		complement["c"] = "g"
		complement["g"] = "c"
		complement["t"] = "a"
		complement["u"] = "a"
		complement["y"] = "r"
		complement["r"] = "y"
		complement["s"] = "s"
		complement["w"] = "w"
		complement["k"] = "m"
		complement["m"] = "k"
		complement["b"] = "v"
		complement["d"] = "h"
		complement["h"] = "d"
		complement["v"] = "b"
		complement["n"] = "n"
	}
	morphism["complement"] = complement
}

// GetMorphism returns expected morphism rules
func GetMorphism(kind string, m string) (map[string]string, error) {
	if morphism == nil {
		morphism = make(map[string]map[string]string)
		initMorphisms(kind)
	}
	morph, ok := morphism[m]
	if !ok {
		return nil, fmt.Errorf("Morphism does not exists: %s", m)
	}
	return morph, nil
}

// BioString represents a DNA/RNA/Protein sequence
type BioString interface {
	GetValue() string
	SetValue(string)
	IsEqual(a rune, b rune) bool
	IsExact(bs BioString) bool
	IsApproximate(bs BioString, subst uint64, indel uint64) (bool, []Match)
	Reverse() BioString
	Morph(m string, reverse bool) (BioString, error)
}

// DnaString defined a DNA sequence with optional allowed morphisms
type DnaString struct {
	value string
}

// GetValue returns sequence string
func (s DnaString) GetValue() string {
	return s.value
}

// SetValue sets sequence string, lower cased
func (s *DnaString) SetValue(seq string) {
	s.value = strings.ToLower(seq)
}

// IsEqual compare to sequence characters
func (s DnaString) IsEqual(a rune, b rune) bool {
	if a == b {
		return true
	}
	switch a {
	case 'n':
		return true
	case 'y':
		if b == 'c' || b == 't' {
			return true
		}
	case 'r':
		if b == 'a' || b == 'g' {
			return true
		}
	case 's':
		if b == 'g' || b == 'c' {
			return true
		}
	case 'w':
		if b == 'a' || b == 't' {
			return true
		}
	case 'k':
		if b == 't' || b == 'u' || b == 'g' {
			return true
		}
	case 'm':
		if b == 'a' || b == 'c' {
			return true
		}
	case 'b':
		if b != 'a' {
			return true
		}
	case 'd':
		if b != 'c' {
			return true
		}
	case 'h':
		if b != 'g' {
			return true
		}
	case 'v':
		if b == 'a' || b == 'c' || b == 'g' {
			return true
		}
	}

	switch b {
	case 'n':
		return true
	case 'y':
		if a == 'c' || a == 't' {
			return true
		}
	case 'r':
		if a == 'a' || a == 'g' {
			return true
		}
	case 's':
		if a == 'g' || a == 'c' {
			return true
		}
	case 'w':
		if a == 'a' || a == 't' {
			return true
		}
	case 'k':
		if a == 't' || a == 'u' || a == 'g' {
			return true
		}
	case 'm':
		if a == 'a' || a == 'c' {
			return true
		}
	case 'b':
		if a != 'a' {
			return true
		}
	case 'd':
		if a != 'c' {
			return true
		}
	case 'h':
		if a != 'g' {
			return true
		}
	case 'v':
		if a == 'a' || a == 'c' || a == 'g' {
			return true
		}
	}

	return false
}

// IsExact checks sequences are equal
func (s DnaString) IsExact(bs BioString) bool {
	// TODO
	otherSeq := bs.GetValue()
	if len(s.value) != len(otherSeq) {
		return false
	}
	other := []rune(otherSeq)
	for index, val := range s.value {
		if !s.IsEqual(val, other[index]) {
			return false
		}
	}
	return true
}

// IsApproximate compare sequence to an other one, accepting substitutions and/or indel
func (s DnaString) IsApproximate(bs BioString, subst uint64, indel uint64) (bool, []Match) {
	results := s.SearchForApproximate(bs, 0, subst, 0, 0, indel, "", "", false, false)
	for index, r := range results {
		results[index].Len = uint64(len(s.GetValue())) + r.Insert - r.Deletion
		results[index].Pattern = r.Value
	}

	uniques := func(input []Match) []Match {
		u := make([]Match, 0, len(input))
		m := make(map[string]Match)
		for _, val := range input {
			key := fmt.Sprintf("%d-%d-%d", val.Cost, val.Insert, val.Deletion)
			if _, ok := m[key]; !ok {
				m[key] = val
				u = append(u, val)
			}
		}
		return u
	}

	hasResults := len(results) > 0
	// Remove duplicates if indel allowed
	if hasResults && indel > 0 {
		results = uniques(results)
	}

	return hasResults, results
}

// Reverse reverse the sequence value
func (s DnaString) Reverse() BioString {
	r := []rune(s.value)
	for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}
	rev := &DnaString{}
	rev.SetValue(string(r))
	return rev
}

// Morph applies a morphism on sequence value, optionally reversing it
func (s DnaString) Morph(m string, reverse bool) (BioString, error) {
	morphs, morphErr := GetMorphism("dna", m)
	if m == "" && reverse {
		return s.Reverse(), nil
	}
	if morphErr != nil {
		newDnaString := DnaString{}
		var newBioString BioString = &newDnaString
		newBioString.SetValue(m)
		return newBioString, morphErr
	}
	morphed := ""
	for i := 0; i < len(s.value); i++ {
		morphed += morphs[string(s.value[i])]
	}

	if reverse {
		rev := []rune(morphed)
		for i, j := 0, len(rev)-1; i < len(rev)/2; i, j = i+1, j-1 {
			rev[i], rev[j] = rev[j], rev[i]
		}
		//revString := DnaString{value: string(rev)}
		revString := DnaString{}
		var newBioString BioString = &revString
		newBioString.SetValue(string(rev))
		return newBioString, nil
	}
	newDnaString := DnaString{}
	var newBioString BioString = &newDnaString
	newBioString.SetValue(morphed)
	return newBioString, nil
}

// SearchForApproximate TODO
func (s DnaString) SearchForApproximate(s2 BioString, cost uint64, maxCost uint64, in uint64, del uint64, maxIndel uint64, data1 string, data2 string, prevDel bool, prevAdd bool) []Match {
	indel := in + del
	logger.Debugf("Start IsApproximate cost: %d, in %d, del %d", cost, in, del)
	s1Len := uint64(len(s.GetValue()))
	s2Len := uint64(len(s2.GetValue()))
	logger.Debugf("Part1:%d %s", s1Len, s.GetValue())
	logger.Debugf("Part2:%d %s", s2Len, s2.GetValue())

	results := make([]Match, 0)
	if s1Len == 0 && s2Len == 0 {
		logger.Debugf("End of comparison, Match! %d;%d;%d", cost, in, del)
		m := Match{
			Len:      uint64(s1Len),
			Cost:     cost,
			Distance: in + del,
			Insert:   in,
			Deletion: del,
			Value:    data1 + " vs " + data2,
		}
		results = append(results, m)
		return results
	}
	if s1Len == 0 && s2Len != 0 {
		if prevDel {
			return results
		}
		if indel >= maxIndel {
			logger.Debugf("End of comparison, Match! %d;%d;%d", cost, in, del)
			m := Match{
				Len:      s1Len,
				Cost:     cost,
				Distance: in + del,
				Insert:   in,
				Deletion: del,
				Value:    data1 + " vs " + data2,
			}
			results = append(results, m)
			return results
		}
		allowedIndels := int(maxIndel - indel)
		if maxIndel-indel >= s2Len {
			allowedIndels = int(s2Len)
		}

		if prevDel {
			return results
		}
		x := ""
		for i := 0; i < allowedIndels; i++ {
			logger.Debugf("End of comparison, Match! %d;%d;%d", cost, in+uint64(i), del)
			x += "+"
			m := Match{
				Len:      s1Len,
				Cost:     cost,
				Distance: in + uint64(i) + del,
				Insert:   in + uint64(i),
				Deletion: del,
				Value:    data1 + x + " vs " + data2 + s2.GetValue()[0:i],
			}
			results = append(results, m)

		}
		return results

	}
	if s1Len != 0 && s2Len == 0 {
		if prevAdd {
			return results
		}
		if indel >= maxIndel {
			logger.Debugf("End of comparison")
			return results
		}
		if maxIndel-indel < s1Len {
			logger.Debugf("End of comparison")
			return results
		}
		logger.Debugf("End of comparison, Match! %d;%d;%d", cost, in, del+s1Len)
		x := ""
		y := ""
		for i := 0; i < int(s1Len); i++ {
			x += "-"
			y += " "
		}
		m := Match{
			Len:      s1Len,
			Cost:     cost,
			Distance: in + del + s1Len,
			Insert:   in,
			Deletion: del + s1Len,
			Value:    data1 + x + " vs " + data2 + y,
		}

		results = append(results, m)
		return results

	}

	logger.Debugf("Compare %s vs %s", string(s.GetValue()[0]), string(s2.GetValue()[0]))
	s1Content := s.GetValue()
	s2Content := s2.GetValue()
	s.SetValue(s.GetValue()[0:1])
	s2.SetValue(s2.GetValue()[0:1])
	if !s.IsExact(s2) {
		logger.Debugf("Cost: %d <? %d", cost, maxCost)
		if cost < maxCost {
			logger.Debugf("Try with cost")
			news1 := &DnaString{}
			news1.SetValue(s1Content[1:])
			news2 := &DnaString{}
			news2.SetValue(s2Content[1:])
			newdata1 := data1 + "X"
			newdata2 := data2 + s2.GetValue()
			tmpRes := news1.SearchForApproximate(news2, cost+1, maxCost, in, del, maxIndel, newdata1, newdata2, false, false)
			results = append(results, tmpRes...)
		}
	} else {
		logger.Debugf("Equal, continue...")
		newdata1 := data1 + s.GetValue()
		newdata2 := data2 + s2.GetValue()
		news1 := &DnaString{}
		news1.SetValue(s1Content[1:])
		news2 := &DnaString{}
		news2.SetValue(s2Content[1:])
		tmpRes := news1.SearchForApproximate(news2, cost, maxCost, in, del, maxIndel, newdata1, newdata2, false, false)
		results = append(results, tmpRes...)
	}
	if indel < maxIndel {
		logger.Debugf("Try with indel")

		news1 := &DnaString{}
		news1.SetValue(s1Content)
		data11 := data1 + "+"
		data21 := data2 + s2.GetValue()

		news2 := &DnaString{}
		if s2Len > 1 {
			news2.SetValue(s2Content[1:])
		}

		if !prevDel {
			tmpRes := news1.SearchForApproximate(news2, cost, maxCost, in+1, del, maxIndel, data11, data21, false, true)
			results = append(results, tmpRes...)
		}

		news1bis := &DnaString{}
		if s1Len > 1 {
			news1bis.SetValue(s1Content[1:])
		}
		news2bis := &DnaString{}
		news2bis.SetValue(s2Content)

		data12 := data1 + "-"
		data22 := data2 + " "

		if !prevAdd {
			tmpRes := news1bis.SearchForApproximate(news2bis, cost, maxCost, in, del+1, maxIndel, data12, data22, true, false)
			results = append(results, tmpRes...)
		}
	}
	logger.Debugf("End of comparison")
	return results
}

/*

// FindApproximate search for matches with errors
func (s SearchUtils) FindApproximate(mch chan logol.Match, grammar logol.Grammar, match logol.Match, model string, modelVariable string, contextVars map[string]logol.Match, spacer bool, maxCost int, maxDistance int) {
	curVariable := grammar.Models[model].Vars[modelVariable]
	if curVariable.Value == "" &&
		curVariable.String_constraints.Content != "" {
		contentConstraint := curVariable.String_constraints.Content
		logger.Debugf("FindExact, get var content %s", contentConstraint)
		curVariable.Value = s.SequenceHandler.GetContent(contextVars[contentConstraint].Start, contextVars[contentConstraint].End)
		logger.Debugf("? %s", curVariable.Value)
		if curVariable.Value == "" {
			close(mch)
			return
		}
	}

	findResults := make([][4]int, 0)
	seqLen := s.SequenceHandler.Sequence.Size
	patternLen := len(curVariable.Value)
	minStart := match.MinPosition
	maxStart := match.MinPosition + 1
	if match.Spacer {
		maxStart = seqLen - patternLen + 1
	}
	if match.Overlap {
		minStartWithDistance := minStart - (patternLen + maxDistance)
		if minStartWithDistance > minStart {
			minStart = minStartWithDistance
			match.MinPosition = minStart
		}
	}

	logger.Debugf("seach between %d and %d", minStart, maxStart)
	for i := minStart; i < maxStart; i++ {
		seqPart := s.SequenceHandler.GetContent(i, i+patternLen+1+maxDistance)

		bioString := NewDnaString(curVariable.Value)
		if match.Reverse {
			curVariable.Value = bioString.Reverse()
		}
		if curVariable.HasMorphism() {
			bioString.SetMorphisms(curVariable.GetMorphism(grammar.Morphisms).Morph)
		}
		b1 := NewDnaString(curVariable.Value)
		approxResults := IsApproximate(&b1, seqPart, 0, maxCost, 0, 0, maxDistance)
		nbApproxResults := len(approxResults)
		if nbApproxResults > 0 {
			for r := 0; r < nbApproxResults; r++ {
				approxResult := approxResults[r]
				length := patternLen + approxResult[2] - approxResult[3]
				elts := [...]int{i, i + length, approxResult[1], approxResult[2] + approxResult[3]}
				findResults = append(findResults, elts)
			}
		}
	}

	uniques := func(input [][4]int) [][4]int {
		u := make([][4]int, 0, len(input))
		m := make(map[string][4]int)
		for _, val := range input {
			key := fmt.Sprintf("%d-%d-%d-%d", val[0], val[1], val[2], val[3])
			if _, ok := m[key]; !ok {
				m[key] = val
				u = append(u, val)
			}
		}
		return u
	}
	// Remove duplicates if indel allowed
	if maxDistance > 0 {
		findResults = uniques(findResults)
	}

	ban := 0
	for _, findResult := range findResults {
		startResult := findResult[0]
		endResult := findResult[1]

		   newMatch := logol.NewMatch()
		   newMatch.Id = modelVariable
		   newMatch.Model = model
		   newMatch.Start = startResult
		   newMatch.End = endResult
		   newMatch.Info = curVariable.Value
		   newMatch.Sub = findResult[2]
		   newMatch.Indel = findResult[3]
		   newMatch.Overlap = match.Overlap
		   newMatch, err := s.PostControl(newMatch, grammar, contextVars)
		   if !err {
			   mch <- newMatch
			   // matches = append(matches, newMatch)
			   logger.Debugf("got match: %d, %d", newMatch.Start, newMatch.End)
		   }
	   }
	   logger.Debugf("got matches: %d", (len(findResults) - ban))

	   close(mch)
   }









*/
