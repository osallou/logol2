package logol

// Match details global match info
type Match struct {
	Model          string
	VarName        string
	Position       uint64           // Position in sequence , starts at 0
	Len            uint64           // Length of match
	Cost           uint64           // Total cost
	Distance       uint64           // Total distance
	Value          string           // String value of match
	Pattern        string           // Comment of variable, optional
	Match          []Match          // Sub elements of match (model calls)
	Vars           map[string]Match // Context variables (model parameters and saved variables)
	Spacer         bool             // Has spacer
	SpacerMin      string           // constraint on spacer when prev variable is undefined
	SpacerMax      string           // constraint on spacer when prev variable is undefined
	SpacerLen      uint64           // Spacer length
	OverlapLen     uint64           //Overlap length
	Defined        bool             // If match could be found or could not be evaluated due to other constraints
	RepeatIndex    uint64           // In case of repeat, index of the repeat
	PrevFoundMatch []Match          // Previously found matches in case of an other run when some vars are undefined

	Insert   uint64
	Deletion uint64
}
