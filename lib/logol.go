package logol

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"reflect"
	"regexp"
	"strings"
)

var logger = GetLogger("logol.lib")

// Models defines mapping between function name and real function
var Models map[string]interface{}

/*
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
*/

// SearchComponent is base interface for all search components
type SearchComponent interface {
	Search() (*Match, error)
	Iterator() (cm chan Match)
	Items()
	GetComponent() Variable
	SaveAs() (string, bool)
	Context() Context
}

// FindExactComponent defines search components
type FindExactComponent struct {
	cm           chan Match
	component    Variable
	ctx          Context
	modelName    string
	varName      string
	prevMatch    Match
	hasPrevMatch bool
}

// FindApproximateComponent search for string with errors
type FindApproximateComponent struct {
	FindExactComponent
}

// Items  TODO
func (fc FindApproximateComponent) Items() {
	logger.Debugf("FindApproximateComponent:Items:%s:%s", fc.modelName, fc.varName)
	if fc.hasPrevMatch && fc.prevMatch.Defined {
		logger.Debugf("FindApproximateComponent: Already found, skipping %+v", fc.prevMatch)
		if fc.Check(fc.prevMatch) {
			fc.cm <- fc.prevMatch
		}
		close(fc.cm)
		return
	}

	_, checkErr := fc.CheckConstraints(Match{}, false)
	if checkErr != nil {
		logger.Debugf("Constraints failed %+v", checkErr)
		match := Match{
			Model:    fc.modelName,
			VarName:  fc.varName,
			Position: fc.ctx.Position,
			Pattern:  fc.component.Comment,
			Defined:  false,
		}
		fc.cm <- match
		close(fc.cm)
		return
	}

	// TODO
	constraint := fc.component.Value

	if fc.component.StringConstraints.Content != "" {
		ctxVar, ok := fc.ctx.Vars[fc.component.StringConstraints.Content]
		if !ok {
			match := Match{
				Model:    fc.modelName,
				VarName:  fc.varName,
				Position: fc.ctx.Position,
				// TODO could check if relates to a constant string or size constrained var to set min size
				Len:     0,
				Pattern: fc.component.Comment,
				Defined: false,
			}
			fc.cm <- match
			close(fc.cm)
			return
		}
		constraint = fc.ctx.SequenceLru.GetContent(int(ctxVar.Position), int(ctxVar.Position+ctxVar.Len))
	}

	if constraint == "" {
		close(fc.cm)
		return
	}

	var minSpacer int64
	var maxSpacer int64
	var maxOverlap int64

	originalPosition := fc.ctx.Position
	// Global spacer
	if fc.ctx.Spacer {
		maxSpacer = int64(fc.ctx.Sequence.Size) - int64(fc.ctx.Position)
	} else {
		if fc.ctx.IsRepeat {
			methSpacerMinName := fc.modelName + "_" + fc.varName + "RepeatSpacerMin"
			methSpacerMaxName := fc.modelName + "_" + fc.varName + "RepeatSpacerMax"
			minSpacerComponent, _ := CallComputeFunction(methSpacerMinName, fc.ctx)
			maxSpacerComponent, _ := CallComputeFunction(methSpacerMaxName, fc.ctx)
			minSpacer = minSpacerComponent
			maxSpacer = maxSpacerComponent
			methOverlapMaxName := fc.modelName + "_" + fc.varName + "RepeatOverlapMax"
			maxOverlap, _ := CallComputeFunction(methOverlapMaxName, fc.ctx)
			fc.ctx.Position = originalPosition - uint64(maxOverlap)
		} else {
			methSpacerMinName := fc.modelName + "_" + fc.varName + "SpacerMin"
			methSpacerMaxName := fc.modelName + "_" + fc.varName + "SpacerMax"
			minSpacerComponent, _ := CallComputeFunction(methSpacerMinName, fc.ctx)
			maxSpacerComponent, _ := CallComputeFunction(methSpacerMaxName, fc.ctx)
			minSpacer = minSpacerComponent
			maxSpacer = maxSpacerComponent
		}
	}

	methStartMinName := fc.modelName + "_" + fc.varName + "StartMin"
	minStart, _ := CallComputeFunction(methStartMinName, fc.ctx)
	if minStart > 0 {
		fc.ctx.Position = uint64(minStart)
	}

	if minSpacer > 0 {
		fc.ctx.Position += uint64(minSpacer)
	}

	methMaxDistanceName := fc.modelName + "_" + fc.varName + "DistanceMax"
	maxDistance, _ := CallComputeFunction(methMaxDistanceName, fc.ctx)

	methMaxCostName := fc.modelName + "_" + fc.varName + "CostMax"
	maxCost, _ := CallComputeFunction(methMaxCostName, fc.ctx)

	logger.Debugf("FindApproximateComponent:Spacer:0:%d, %+v", maxSpacer, fc.ctx)
	for i := 0; i <= int(maxOverlap+maxSpacer); i++ {

		content := fc.ctx.SequenceLru.GetContent(int(fc.ctx.Position), int(fc.ctx.Position)+len(constraint)+int(maxDistance))
		if content == "" {
			logger.Debugf("FindApproximateComponent:no match found")
			close(fc.cm)
			return
		}
		logger.Debugf("FindApproximateComponent:Compare %s vs %s", constraint, content)

		seq1 := &DnaString{}
		seq1.SetValue(constraint)
		seq2 := &DnaString{}
		seq2.SetValue(content)
		var matches []Match
		hasMatches := false
		if fc.component.StringConstraints.Morphism != "" {
			morphseq1, morphErr := seq1.Morph(fc.component.StringConstraints.Morphism, fc.component.StringConstraints.Reverse)
			if morphErr != nil {
				logger.Errorf("Morph error %+v", morphErr)
				close(fc.cm)
				return
			}
			hasMatches, matches = morphseq1.IsApproximate(seq2, uint64(maxCost), uint64(maxDistance))
		} else {
			hasMatches, matches = seq1.IsApproximate(seq2, uint64(maxCost), uint64(maxDistance))
		}
		logger.Debugf("FindApproximate results: %+v", matches)
		if hasMatches {
			for _, m := range matches {
				match := Match{
					Model:          fc.modelName,
					VarName:        fc.varName,
					Value:          content[0:m.Len],
					Position:       fc.ctx.Position,
					Len:            m.Len,
					Cost:           m.Cost,
					Distance:       m.Distance,
					Vars:           fc.ctx.Vars,
					PrevFoundMatch: fc.prevMatch.PrevFoundMatch,
					Defined:        true,
					Pattern:        m.Pattern,
				}
				match.SpacerLen = match.Position - originalPosition
				if match.Position < originalPosition {
					match.OverlapLen = originalPosition - match.Position
				}
				checked, checkErr := fc.CheckConstraints(match, true)
				if checkErr == nil && checked {
					fc.cm <- match
				}
			}
		}

		fc.ctx.Position++
	}
	close(fc.cm)
}

// FindModelComponent calls an other model
type FindModelComponent struct {
	FindExactComponent
}

// Items TODO
func (fc FindModelComponent) Items() {
	logger.Debugf("FindModelComponent:%s:%s", fc.modelName, fc.varName)
	calledModel := fc.ctx.Grammar.Models[fc.component.Model.Name]
	calledModelName := fc.component.Model.Name
	fn, ok := Models[calledModelName]
	if !ok {
		logger.Errorf("Model %s not defined", calledModelName)
		close(fc.cm)
		return
	}
	chin := make(chan Match)
	chout := make(chan Match)
	newCtx := fc.ctx
	newCtx.Vars = make(map[string]Match)

	methStartMinName := fc.modelName + "_" + fc.varName + "StartMin"
	minStart, _ := CallComputeFunction(methStartMinName, fc.ctx)
	if minStart > 0 {
		fc.ctx.Position = uint64(minStart)
	}

	matchStart := Match{
		Model:    fc.modelName,
		VarName:  fc.varName,
		Value:    "init",
		Position: fc.ctx.Position,
		Match:    make([]Match, 0),
		Vars:     make(map[string]Match),
	}

	if fc.hasPrevMatch {
		matchStart.PrevFoundMatch = fc.prevMatch.PrevFoundMatch
	}

	if fc.ctx.Spacer {
		matchStart.Spacer = true
	}
	if fc.component.Spacer {
		matchStart.Spacer = true
	}
	// TODO manage spacer for var

	// set Vars from current parameters
	calledModelParams := calledModel.Param
	for index, param := range calledModelParams {
		callingVar := fc.component.Model.Param[index]
		callingVarDetails, ok := fc.ctx.Vars[callingVar]
		if ok {
			matchStart.Vars[param] = callingVarDetails
			newCtx.Vars[param] = callingVarDetails
		}
	}

	fnc := reflect.ValueOf(fn)
	in := make([]reflect.Value, 3)
	in[0] = reflect.ValueOf(chin)
	in[1] = reflect.ValueOf(chout)
	in[2] = reflect.ValueOf(newCtx)
	logger.Debugf("Call %s with context %+v", calledModelName, newCtx)
	go fnc.Call(in)
	chin <- matchStart
	close(chin)

	foundMatches := make(map[int64]bool)
	for msg := range chout {
		logger.Debugf("Got result message from model %s: %+v", calledModelName, msg)
		match := MergeMatches(msg, fc.Context(), fc.component)

		match.Model = fc.modelName
		match.VarName = fc.varName
		// update vars with received parameters
		for index, param := range calledModelParams {
			callingVar := fc.component.Model.Param[index]
			calledVarDetails, ok := msg.Vars[param]
			if ok {
				match.Vars[callingVar] = calledVarDetails
			}
		}
		logger.Debugf("Updated message from model %s: %+v", calledModelName, match)
		foundMatches[int64(match.Len)] = true
		if !fc.component.Not {
			checked, checkErr := fc.CheckConstraints(match, true)
			if checkErr == nil && checked {
				fc.cm <- match
			}
		}
	}
	// check/manage Not case
	// look over min and max of size
	// if size not in foundmatches send it to fc.cm
	if fc.component.Not {
		fail := false
		// minSize := int64(0)
		// maxSize := int64(0)

		methMinName := fc.modelName + "_" + fc.varName + "SizeMin"
		methMaxName := fc.modelName + "_" + fc.varName + "SizeMax"
		minSize, minerr := CallComputeFunction(methMinName, fc.ctx)
		maxSize, maxerr := CallComputeFunction(methMaxName, fc.ctx)
		if minerr != nil || maxerr != nil {
			fail = true
			logger.Errorf("Failed to evaluate conditions: %s, %s", minerr, maxerr)
		}

		if !fail {
			for i := minSize; i <= maxSize; i++ {
				if _, ok := foundMatches[minSize]; ok {
					continue
				}
				content := fc.ctx.SequenceLru.GetContent(int(minSize), int(maxSize))
				match := Match{
					Model:          fc.modelName,
					VarName:        fc.varName,
					Value:          content,
					Position:       fc.ctx.Position,
					Len:            uint64(i),
					Vars:           fc.ctx.Vars,
					PrevFoundMatch: fc.prevMatch.PrevFoundMatch,
				}
				checked, checkErr := fc.CheckConstraints(match, true)
				if checkErr == nil && checked {
					fc.cm <- match
				}
				minSize++
			}
		}
	}

	logger.Debugf("Exiting from model call %s", calledModelName)
	close(fc.cm)
}

// FindSizeRangeComponent defines component to get a string between min and max size
type FindSizeRangeComponent struct {
	FindExactComponent
	count int64
	min   int64
	max   int64
}

// Items loop over possible matches and send matches to iterator chan
func (fc FindSizeRangeComponent) Items() {
	logger.Debugf("FindSizeRangeComponent:%s:%s", fc.modelName, fc.varName)
	if fc.hasPrevMatch && fc.prevMatch.Defined {
		// TODO check for position, taking into account spacer/overlap
		logger.Debugf("FindSizeRangeComponent: Already found, skipping %+v", fc.prevMatch)
		if fc.Check(fc.prevMatch) {
			fc.cm <- fc.prevMatch
		}
		close(fc.cm)
		return
	}

	_, checkErr := fc.CheckConstraints(Match{}, false)
	if checkErr != nil {
		match := Match{
			Model:    fc.modelName,
			VarName:  fc.varName,
			Position: fc.ctx.Position,
			Pattern:  fc.component.Comment,
			Defined:  false,
		}
		fc.cm <- match
		close(fc.cm)
		return
	}

	var err error
	var match *Match

	methStartMinName := fc.modelName + "_" + fc.varName + "StartMin"
	minStart, _ := CallComputeFunction(methStartMinName, fc.ctx)
	if minStart > 0 {
		fc.ctx.Position = uint64(minStart)
	}

	methMinName := fc.modelName + "_" + fc.varName + "SizeMin"
	methMaxName := fc.modelName + "_" + fc.varName + "SizeMax"
	min, minerr := CallComputeFunction(methMinName, fc.ctx)
	max, maxerr := CallComputeFunction(methMaxName, fc.ctx)
	if minerr != nil || maxerr != nil {
		fc.cm <- Match{
			Model:    fc.modelName,
			VarName:  fc.varName,
			Position: fc.ctx.Position,
			Pattern:  fc.component.Comment,
			Len:      0,
			Defined:  false,
		}
		close(fc.cm)
		return
	}
	fc.min = min
	fc.max = max
	fc.count = min

	maxSpacer := 0
	// Global spacer
	if fc.ctx.Spacer {
		maxSpacer = fc.ctx.Sequence.Size - int(fc.ctx.Position)
	}

	originalPosition := fc.ctx.Position
	for i := 0; i <= maxSpacer; i++ {
		for size := fc.min; size <= fc.max; size++ {
			match, err = fc.Search()
			if err == nil {
				match.SpacerLen = match.Position - originalPosition
				checked, checkErr := fc.CheckConstraints(*match, true)
				if checkErr == nil && checked {
					fc.cm <- *match
				}
			}
		}
		fc.ctx.Position++
	}
	close(fc.cm)
}

// Search search for all possible matches
func (fc FindSizeRangeComponent) Search() (*Match, error) {
	// call function minsize maxsize from Models
	logger.Debugf("FindSizeRangeComponent:Search:%s:%s:FindModelComponent", fc.modelName, fc.varName)

	if fc.count > fc.max {
		return nil, fmt.Errorf("Max reached")
	}
	logger.Debugf("FindSizeRangeComponent:Range:%d:%d", fc.min, fc.max)

	content := fc.ctx.SequenceLru.GetContent(int(fc.ctx.Position), int(fc.ctx.Position)+int(fc.count))
	if content == "" {
		return nil, fmt.Errorf("FindExactComponent:no match found")
	}

	match := Match{
		Model:    fc.modelName,
		VarName:  fc.varName,
		Position: fc.ctx.Position,
		Len:      uint64(len(content)),
		Value:    content,
		Pattern:  fc.component.Comment,
		Defined:  true,
	}
	return &match, nil
}

// NewSearchComponent TODO
func NewSearchComponent(ctx Context, modelName string, varName string, hasPrevMatch bool, prevMatch Match) SearchComponent {
	if ctx.Grammar.Models[modelName].Vars[varName].Model.Name != "" {
		// Call an other model
		logger.Debugf("Component:%s:%s:FindModelComponent", modelName, varName)
		fc := FindModelComponent{}
		fc.modelName = modelName
		fc.varName = varName
		fc.hasPrevMatch = hasPrevMatch
		fc.prevMatch = prevMatch
		fc.cm = make(chan Match)
		if ctx.Grammar != nil {
			fc.component = ctx.Grammar.Models[modelName].Vars[varName]
		}
		fc.ctx = ctx
		return fc
	} else if ctx.Grammar.Models[modelName].Vars[varName].Value != "" || ctx.Grammar.Models[modelName].Vars[varName].StringConstraints.Content != "" {
		variable := ctx.Grammar.Models[modelName].Vars[varName]
		if variable.StructConstraints.Cost.Max != "" || variable.StructConstraints.Distance.Max != "" {
			logger.Debugf("Component:%s:%s:FindApproximateComponent", modelName, varName)
			fc := FindApproximateComponent{}
			fc.hasPrevMatch = hasPrevMatch
			fc.prevMatch = prevMatch
			fc.modelName = modelName
			fc.varName = varName
			fc.cm = make(chan Match)
			if ctx.Grammar != nil {
				fc.component = ctx.Grammar.Models[modelName].Vars[varName]
			}
			fc.ctx = ctx
			return fc
		}
		// Exact  match
		logger.Debugf("Component:%s:%s:FindExactComponent", modelName, varName)
		fc := FindExactComponent{}
		fc.hasPrevMatch = hasPrevMatch
		fc.prevMatch = prevMatch
		fc.modelName = modelName
		fc.varName = varName
		fc.cm = make(chan Match)
		if ctx.Grammar != nil {
			fc.component = ctx.Grammar.Models[modelName].Vars[varName]
		}
		fc.ctx = ctx
		return fc
	} else if ctx.Grammar.Models[modelName].Vars[varName].StringConstraints.Size.Min != "" && ctx.Grammar.Models[modelName].Vars[varName].StringConstraints.Size.Max != "" {
		// Size based variable
		logger.Debugf("Component:%s:%s:SizeRangeComponent", modelName, varName)
		fc := FindSizeRangeComponent{}
		fc.hasPrevMatch = hasPrevMatch
		fc.prevMatch = prevMatch
		fc.modelName = modelName
		fc.varName = varName
		fc.cm = make(chan Match)
		if ctx.Grammar != nil {
			fc.component = ctx.Grammar.Models[modelName].Vars[varName]
		}
		fc.ctx = ctx
		return fc
	}
	panic(fmt.Sprintf("Failed to find a component, exiting, %+v", ctx.Grammar.Models[modelName].Vars[varName]))

}

// CheckConstraints checks if match constraints can be evaluated and, if eval is true, checks constraints
func (fc FindExactComponent) CheckConstraints(match Match, eval bool) (bool, error) {
	logger.Debugf("CheckConstraints:%s:%s, eval=%t", fc.modelName, fc.varName, eval)
	methSpacerMinName := fc.modelName + "_" + fc.varName + "SpacerMin"
	methSpacerMaxName := fc.modelName + "_" + fc.varName + "SpacerMax"
	minSpacer, minSpacerErr := CallComputeFunction(methSpacerMinName, fc.ctx)
	maxSpacer, maxSpacerErr := CallComputeFunction(methSpacerMaxName, fc.ctx)
	if minSpacerErr != nil || maxSpacerErr != nil {
		return false, fmt.Errorf("Cannot determine spacer")
	}

	methStartMinName := fc.modelName + "_" + fc.varName + "StartMin"
	methStartMaxName := fc.modelName + "_" + fc.varName + "StartMax"
	minStart, minStartErr := CallComputeFunction(methStartMinName, fc.ctx)
	maxStart, maxStartErr := CallComputeFunction(methStartMaxName, fc.ctx)
	if minStartErr != nil || maxStartErr != nil {
		return false, fmt.Errorf("Cannot determine start")
	}

	methEndMinName := fc.modelName + "_" + fc.varName + "EndMin"
	methEndMaxName := fc.modelName + "_" + fc.varName + "EndMax"
	minEnd, minEndErr := CallComputeFunction(methEndMinName, fc.ctx)
	maxEnd, maxEndErr := CallComputeFunction(methEndMaxName, fc.ctx)
	if minEndErr != nil || maxEndErr != nil {
		return false, fmt.Errorf("Cannot determine end")
	}

	methSizeMinName := fc.modelName + "_" + fc.varName + "SizeMin"
	methSizeMaxName := fc.modelName + "_" + fc.varName + "SizeMax"
	minSize, minSizeErr := CallComputeFunction(methSizeMinName, fc.ctx)
	maxSize, maxSizeErr := CallComputeFunction(methSizeMaxName, fc.ctx)
	if minSizeErr != nil || maxSizeErr != nil {
		return false, fmt.Errorf("Cannot determine size")
	}

	methCostMinName := fc.modelName + "_" + fc.varName + "CostMin"
	methCostMaxName := fc.modelName + "_" + fc.varName + "CostMax"
	minCost, minCostErr := CallComputeFunction(methCostMinName, fc.ctx)
	maxCost, maxCostErr := CallComputeFunction(methCostMaxName, fc.ctx)
	if minCostErr != nil || maxCostErr != nil {
		return false, fmt.Errorf("Cannot determine cost")
	}

	methDistanceMinName := fc.modelName + "_" + fc.varName + "DistanceMin"
	methDistanceMaxName := fc.modelName + "_" + fc.varName + "DistanceMax"
	minDistance, minDistanceErr := CallComputeFunction(methDistanceMinName, fc.ctx)
	maxDistance, maxDistanceErr := CallComputeFunction(methDistanceMaxName, fc.ctx)
	if minDistanceErr != nil || maxDistanceErr != nil {
		return false, fmt.Errorf("Cannot determine distance")
	}

	methRepeatMinName := fc.modelName + "_" + fc.varName + "RepeatMin"
	methRepeatMaxName := fc.modelName + "_" + fc.varName + "RepeatMax"
	_, minRepeatErr := CallComputeFunction(methRepeatMinName, fc.ctx)
	maxRepeat, maxRepeatErr := CallComputeFunction(methRepeatMaxName, fc.ctx)
	if minRepeatErr != nil || maxRepeatErr != nil {
		return false, fmt.Errorf("Cannot determine repeat")
	}

	methRepeatSpacerMinName := fc.modelName + "_" + fc.varName + "RepeatSpacerMin"
	methRepeatSpacerMaxName := fc.modelName + "_" + fc.varName + "RepeatSpacerMax"
	minRepeatSpacer, minRepeatSpacerErr := CallComputeFunction(methRepeatSpacerMinName, fc.ctx)
	maxRepeatSpacer, maxRepeatSpacerErr := CallComputeFunction(methRepeatSpacerMaxName, fc.ctx)
	if minRepeatSpacerErr != nil || maxRepeatSpacerErr != nil {
		return false, fmt.Errorf("Cannot determine repeat spacer")
	}

	methRepeatOverlapMinName := fc.modelName + "_" + fc.varName + "RepeatOverlapMin"
	methRepeatOverlapMaxName := fc.modelName + "_" + fc.varName + "RepeatOverlapMax"
	minRepeatOverlap, minRepeatOverlapErr := CallComputeFunction(methRepeatOverlapMinName, fc.ctx)
	maxRepeatOverlap, maxRepeatOverlapErr := CallComputeFunction(methRepeatOverlapMaxName, fc.ctx)
	if minRepeatOverlapErr != nil || maxRepeatOverlapErr != nil {
		return false, fmt.Errorf("Cannot determine repeat spacer")
	}

	methOverlapMinName := fc.modelName + "_" + fc.varName + "OverlapMin"
	methOverlapMaxName := fc.modelName + "_" + fc.varName + "OverlapMax"
	_, minOverlapErr := CallComputeFunction(methOverlapMinName, fc.ctx)
	_, maxOverlapErr := CallComputeFunction(methOverlapMaxName, fc.ctx)
	if minOverlapErr != nil || maxOverlapErr != nil {
		return false, fmt.Errorf("Cannot determine overlap")
	}

	if !eval {
		return true, nil
	}
	if minSpacer > 0 && match.SpacerLen < uint64(minSpacer) {
		logger.Debugf("minSpacer control failed %d < %d", match.SpacerLen, minSpacer)
		return false, nil
	}
	if maxSpacer > 0 && match.SpacerLen > uint64(maxSpacer) {
		logger.Debugf("maxSpacer control failed %d > %d", match.SpacerLen, maxSpacer)
		return false, nil
	}

	if minStart > 0 && match.Position < uint64(minStart) {
		logger.Debugf("minStart control failed %d < %d", match.Position, minStart)
		return false, nil
	}
	if maxStart > 0 && match.Position > uint64(maxStart) {
		logger.Debugf("maxSpacer control failed %d > %d", match.Position, maxStart)
		return false, nil
	}

	if minEnd > 0 && match.Position+match.Len < uint64(minEnd) {
		logger.Debugf("minEnd control failed %d < %d", match.Position+match.Len, minEnd)
		return false, nil
	}
	if maxEnd > 0 && match.Position+match.Len > uint64(maxEnd) {
		logger.Debugf("maxEnd control failed %d > %d", match.Position+match.Len, maxEnd)
		return false, nil
	}

	if minSize > 0 && match.Len < uint64(minSize) {
		logger.Debugf("minSize control failed %d < %d", match.Len, minSize)
		return false, nil
	}
	if maxSize > 0 && match.Len > uint64(maxSize) {
		logger.Debugf("maxSize control failed %d > %d", match.Len, maxSize)
		return false, nil
	}

	if minCost > 0 && match.Cost < uint64(minCost) {
		logger.Debugf("minCost control failed %d < %d", match.Cost, minCost)
		return false, nil
	}
	if maxCost > 0 && match.Cost > uint64(maxCost) {
		logger.Debugf("maxCost control failed %d > %d", match.Cost, maxCost)
		return false, nil
	}

	if minDistance > 0 && match.Distance < uint64(minDistance) {
		logger.Debugf("minDist control failed %d < %d", match.Distance, minDistance)
		return false, nil
	}
	if maxDistance > 0 && match.Distance > uint64(maxDistance) {
		logger.Debugf("maxDist control failed %d > %d", match.Distance, maxDistance)
		return false, nil
	}

	// Check max repeat only here
	if maxRepeat > 0 && match.RepeatIndex > uint64(maxRepeat) {
		logger.Debugf("maxRepeat control failed %d > %d", match.RepeatIndex, maxRepeat)
		return false, nil
	}

	if match.RepeatIndex > 0 {
		// Check repeats
		if minRepeatSpacer > 0 && match.SpacerLen < uint64(minRepeatSpacer) {
			logger.Debugf("minRepeatSpacer control failed %d < %d", match.SpacerLen, minRepeatSpacer)
			return false, nil
		}
		if maxRepeatSpacer > 0 && match.SpacerLen > uint64(maxRepeatSpacer) {
			logger.Debugf("maxRepeatSpacer control failed %d > %d", match.SpacerLen, maxRepeatSpacer)
			return false, nil
		}
		if minRepeatOverlap > 0 && match.OverlapLen < uint64(minRepeatOverlap) {
			logger.Debugf("minRepeatOverlap control failed %d < %d", match.OverlapLen, minRepeatOverlap)
			return false, nil
		}
		if maxRepeatOverlap > 0 && match.OverlapLen < uint64(maxRepeatOverlap) {
			logger.Debugf("maxRepeatOverlap control failed %d > %d", match.OverlapLen, maxRepeatOverlap)
			return false, nil
		}
	}

	logger.Debugf("CheckConstraints:%s:%s:OK", fc.modelName, fc.varName)
	return true, nil

}

// Check verifies match position against ctx position and constraints
func (fc FindExactComponent) Check(m Match) bool {
	if fc.ctx.Spacer {
		return true
	}
	// Component spacer
	spacer := false
	methMinName := fc.modelName + "_" + fc.varName + "SpacerMin"
	methMaxName := fc.modelName + "_" + fc.varName + "SpacerMax"
	minSize, _ := CallComputeFunction(methMinName, fc.ctx)
	maxSize, _ := CallComputeFunction(methMaxName, fc.ctx)
	if minSize > 0 && m.Position < fc.ctx.Position+uint64(minSize) {
		logger.Debugf("Check on spacer failed")
		return false
	}
	if maxSize > 0 && m.Position > fc.ctx.Position+uint64(maxSize) {
		logger.Debugf("Check on spacer failed")
		return false
	}
	if minSize > 0 || maxSize > 0 {
		spacer = true
	}

	// Component overlap
	overlap := false
	methMinName = fc.modelName + "_" + fc.varName + "OverlapMin"
	methMaxName = fc.modelName + "_" + fc.varName + "OverlapMax"
	minSize, _ = CallComputeFunction(methMinName, fc.ctx)
	maxSize, _ = CallComputeFunction(methMaxName, fc.ctx)
	if minSize > 0 && m.Position > fc.ctx.Position-uint64(minSize) {
		logger.Debugf("Check on overlap failed")
		return false
	}
	if maxSize > 0 && m.Position < fc.ctx.Position-uint64(maxSize) {
		logger.Debugf("Check on overlap failed")
		return false
	}
	if minSize > 0 || maxSize > 0 {
		overlap = true
	}

	methStartMinName := fc.modelName + "_" + fc.varName + "StartMin"
	minStart, _ := CallComputeFunction(methStartMinName, fc.ctx)
	if fc.ctx.Position < uint64(minStart) {
		logger.Debugf("Check on min start failed")
		return false
	}
	methStartMaxName := fc.modelName + "_" + fc.varName + "StartMax"
	maxStart, _ := CallComputeFunction(methStartMaxName, fc.ctx)
	if fc.ctx.Position < uint64(maxStart) {
		logger.Debugf("Check on max start failed")
		return false
	}

	if m.RepeatIndex > 0 {
		// This is a repeated element, check repeat spacer/overlap
		methMinName := fc.modelName + "_" + fc.varName + "RepeatSpacerMin"
		methMaxName := fc.modelName + "_" + fc.varName + "RepeatSpacerMax"
		minSize, _ := CallComputeFunction(methMinName, fc.ctx)
		maxSize, _ := CallComputeFunction(methMaxName, fc.ctx)
		if minSize > 0 && m.Position < fc.ctx.Position+uint64(minSize) {
			logger.Debugf("Check on spacer failed")
			return false
		}
		if maxSize > 0 && m.Position > fc.ctx.Position+uint64(maxSize) {
			logger.Debugf("Check on spacer failed")
			return false
		}
		if minSize > 0 || maxSize > 0 {
			spacer = true
		}

		// Component overlap
		methMinName = fc.modelName + "_" + fc.varName + "RepeatOverlapMin"
		methMaxName = fc.modelName + "_" + fc.varName + "RepeatOverlapMax"
		minSize, _ = CallComputeFunction(methMinName, fc.ctx)
		maxSize, _ = CallComputeFunction(methMaxName, fc.ctx)
		if minSize > 0 && m.Position > fc.ctx.Position-uint64(minSize) {
			logger.Debugf("Check on overlap failed")
			return false
		}
		if maxSize > 0 && m.Position < fc.ctx.Position-uint64(maxSize) {
			logger.Debugf("Check on overlap failed")
			return false
		}
		if minSize > 0 || maxSize > 0 {
			overlap = true
		}
	}

	if !spacer && !overlap && fc.ctx.Position != m.Position {
		logger.Debugf("Check on position failed")
		return false
	}

	logger.Debugf("Match do not match on position, skipping %+v", m)
	return true
}

// GetComponent TODO
func (fc FindExactComponent) GetComponent() Variable {
	return fc.component
}

// Context returns ctx element
func (fc FindExactComponent) Context() Context {
	return fc.ctx
}

// Search TODO
func (fc FindExactComponent) Search() (*Match, error) {
	logger.Debugf("FindExactComponent:Search:%s:%s", fc.modelName, fc.varName)

	constraint := fc.component.Value

	if fc.component.StringConstraints.Content != "" {
		ctxVar, ok := fc.ctx.Vars[fc.component.StringConstraints.Content]
		if !ok {
			match := Match{
				Model:    fc.modelName,
				VarName:  fc.varName,
				Position: fc.ctx.Position,
				// TODO could check if relates to a constant string or size constrained var to set min size
				Len:     0,
				Pattern: fc.component.Comment,
				Defined: false,
			}
			return &match, nil
		}
		constraint = fc.ctx.SequenceLru.GetContent(int(ctxVar.Position), int(ctxVar.Position+ctxVar.Len))
	}

	if constraint == "" {
		return nil, fmt.Errorf("No content found")
	}

	content := fc.ctx.SequenceLru.GetContent(int(fc.ctx.Position), int(fc.ctx.Position)+len(constraint))
	if content == "" {
		return nil, fmt.Errorf("FindExactComponent:no match found")
	}
	logger.Debugf("FindExactComponent:Compare %s vs %s", constraint, content)

	seq1 := &DnaString{}
	seq1.SetValue(constraint)
	seq2 := &DnaString{}
	seq2.SetValue(content)
	isEqual := false
	if fc.component.StringConstraints.Morphism != "" {
		morphseq1, morphErr := seq1.Morph(fc.component.StringConstraints.Morphism, fc.component.StringConstraints.Reverse)
		if morphErr != nil {
			return nil, morphErr
		}
		isEqual = morphseq1.IsExact(seq2)
	} else {
		isEqual = seq1.IsExact(seq2)
	}

	if isEqual {
		match := Match{
			Model:    fc.modelName,
			VarName:  fc.varName,
			Position: fc.ctx.Position,
			Len:      uint64(len(content)),
			Value:    content,
			Pattern:  fc.component.Comment,
			Defined:  true,
		}
		logger.Debugf("FindExactComponent:Got match %+v", match)
		return &match, nil
	}
	return nil, fmt.Errorf("FindExactComponent:no match found")
}

// SaveAs tells us if component should save variable and with which variable name
func (fc FindExactComponent) SaveAs() (string, bool) {
	if fc.component.StringConstraints.SaveAs != "" {
		return fc.component.StringConstraints.SaveAs, true
	}
	return "", false
}

// Iterator returns a chan to receive matches
func (fc FindExactComponent) Iterator() (cm chan Match) {
	return fc.cm
}

// Items loop over possible matches and send matches to iterator chan
func (fc FindExactComponent) Items() {
	logger.Debugf("FindExactComponent:%s:%s", fc.modelName, fc.varName)
	if fc.hasPrevMatch && fc.prevMatch.Defined {
		// TODO check for position, taking into account spacer/overlap
		logger.Debugf("FindExactComponent: Already found, skipping %+v", fc.prevMatch)
		if fc.Check(fc.prevMatch) {
			fc.cm <- fc.prevMatch
		}
		close(fc.cm)
		return
	}

	_, checkErr := fc.CheckConstraints(Match{}, false)
	if checkErr != nil {
		match := Match{
			Model:    fc.modelName,
			VarName:  fc.varName,
			Position: fc.ctx.Position,
			Pattern:  fc.component.Comment,
			Defined:  false,
		}
		fc.cm <- match
		close(fc.cm)
		return
	}

	// TODO not efficient spacer use for exact match, should use cassiopee
	var maxSpacer int64
	var minSpacer int64

	methStartMinName := fc.modelName + "_" + fc.varName + "StartMin"
	minStart, _ := CallComputeFunction(methStartMinName, fc.ctx)
	if minStart > 0 {
		fc.ctx.Position = uint64(minStart)
	}

	originalPosition := fc.ctx.Position

	methOverlapMaxName := fc.modelName + "_" + fc.varName + "OverlapMax"
	maxOverlap, _ := CallComputeFunction(methOverlapMaxName, fc.ctx)
	fc.ctx.Position -= uint64(maxOverlap)

	// Global spacer
	if fc.ctx.Spacer {
		maxSpacer = int64(fc.ctx.Sequence.Size) - int64(fc.ctx.Position)
	} else {
		if fc.ctx.IsRepeat {
			methSpacerMinName := fc.modelName + "_" + fc.varName + "RepeatSpacerMin"
			methSpacerMaxName := fc.modelName + "_" + fc.varName + "RepeatSpacerMax"
			minSpacerComponent, _ := CallComputeFunction(methSpacerMinName, fc.ctx)
			maxSpacerComponent, _ := CallComputeFunction(methSpacerMaxName, fc.ctx)
			minSpacer = minSpacerComponent
			maxSpacer = maxSpacerComponent
			methOverlapMaxName := fc.modelName + "_" + fc.varName + "RepeatOverlapMax"
			maxOverlap, _ := CallComputeFunction(methOverlapMaxName, fc.ctx)
			fc.ctx.Position = originalPosition - uint64(maxOverlap)
		} else {
			methSpacerMinName := fc.modelName + "_" + fc.varName + "SpacerMin"
			methSpacerMaxName := fc.modelName + "_" + fc.varName + "SpacerMax"
			minSpacerComponent, _ := CallComputeFunction(methSpacerMinName, fc.ctx)
			maxSpacerComponent, _ := CallComputeFunction(methSpacerMaxName, fc.ctx)
			minSpacer = minSpacerComponent
			maxSpacer = maxSpacerComponent
		}
	}
	if minSpacer > 0 {
		fc.ctx.Position += uint64(minSpacer)
	}

	logger.Debugf("FindExactComponent:Spacer:0:%d, %+v", maxSpacer, fc.ctx)
	for i := 0; i <= int(maxOverlap+maxSpacer); i++ {
		match, err := fc.Search()
		if err != nil {
			logger.Debugf("no match found %+v", err)
		} else {
			match.SpacerLen = match.Position - originalPosition
			if match.Position < originalPosition {
				match.OverlapLen = originalPosition - match.Position
			}
			checked, checkErr := fc.CheckConstraints(*match, true)
			if checkErr == nil && checked {
				fc.cm <- *match
			}
		}
		fc.ctx.Position++
	}
	close(fc.cm)
}

// Stat contains stat info for progress
type Stat struct {
	Trigger uint64
	Matches uint64
}

var progress = make(map[string]*Stat)

// TrackProgress increment number of input message per component
func TrackProgress(p chan string) {
	for pg := range p {
		stat := strings.Split(pg, ":")
		if _, ok := progress[stat[0]]; !ok {
			progress[stat[0]] = &Stat{}
		}
		if stat[1] == "T" {
			progress[stat[0]].Trigger++
		} else if stat[1] == "M" {
			progress[stat[0]].Matches++
		}
	}
}

// GetProgress returns progress count
func GetProgress() map[string]*Stat {
	return progress
}

func progressHandler(w http.ResponseWriter, r *http.Request) {
	js, err := json.Marshal(progress)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func modelHandler(w http.ResponseWriter, r *http.Request) {
	page := `
	<div id="container" style="height:400px; margin: auto; max-width:800px"></div>
	<script src="https://cdn.jsdelivr.net/npm/sigma@1.2.1/build/sigma.require.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sigma@1.2.1/plugins/sigma.layout.forceAtlas2/supervisor.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sigma@1.2.1/plugins/sigma.layout.forceAtlas2/worker.js"></script>
	<script>
	`
	page += "data = " + graphJSON + ";\n"
	page += `
	var s = new sigma(
		{
		  renderer: {
			container: document.getElementById('container'),
			type: 'canvas'
		  },
		  settings: {
			minArrowSize: 10
		  }
		}
	  );
	  // load the graph
		s.graph.read(data);
		// draw the graph
		s.refresh();
		s.startForceAtlas2();
        window.setTimeout(function() {s.killForceAtlas2()}, 1000);
	</script>
	
	`
	//w.Header().Set("Content-Type", "text/html")
	w.Write([]byte(page))
}

var graphJSON string

// ServeProgress starts a background web server to query progress
func ServeProgress(graph string) {
	graphJSON = graph
	http.HandleFunc("/model", modelHandler)
	http.HandleFunc("/progress", progressHandler)
	port := os.Getenv("LOGOL_LISTEN") // ":8080 for example"
	if port != "" {
		log.Fatal(http.ListenAndServe(port, nil))
	}
}

// MergeRepeats merge a match with its previous repeat matches
func MergeRepeats(ctx Context, msg Match, prevMsg []Match) Match {
	if len(prevMsg) == 0 {
		return msg
	}
	allMatches := append(prevMsg, msg)
	newMatch := msg
	newMatch.Spacer = prevMsg[0].Spacer
	newMatch.SpacerLen = prevMsg[0].SpacerLen
	newMatch.Position = prevMsg[0].Position
	newMatch.Len = 0
	newMatch.Cost = 0
	newMatch.Distance = 0
	newMatch.Match = make([]Match, 0)
	for _, elt := range allMatches {
		newMatch.Len += elt.Len
		newMatch.Cost += elt.Cost
		newMatch.Distance += elt.Distance
	}
	newMatch.Value = ctx.SequenceLru.GetContent(int(newMatch.Position), int(newMatch.Position+newMatch.Len))
	return newMatch
}

// MergeMatches merges multiple match results in a single match
func MergeMatches(msg Match, ctx Context, component Variable) Match {
	firstMatch := msg.Match[0]
	lastMatch := msg.Match[len(msg.Match)-1]
	var endPosition uint64
	if lastMatch.Defined {
		endPosition = lastMatch.Position + lastMatch.Len
	}
	position := msg.Position
	if firstMatch.Defined {
		position = firstMatch.Position
	}
	var matchLen uint64
	content := ""
	if firstMatch.Defined && lastMatch.Defined {
		matchLen = endPosition - position
		content = ctx.SequenceLru.GetContent(int(firstMatch.Position), int(lastMatch.Position+lastMatch.Len))
	}

	// Check if all sub elements are defined
	allDefined := true
	var cost uint64
	var distance uint64
	for _, subm := range msg.Match {
		if !subm.Defined {
			allDefined = false
			break
		}
		cost += subm.Cost
		distance += subm.Distance
	}

	match := Match{
		Position:    position,
		Len:         matchLen,
		Value:       content,
		Defined:     allDefined,
		Pattern:     component.Comment,
		Match:       msg.Match,
		Spacer:      firstMatch.Spacer,
		SpacerLen:   firstMatch.SpacerLen,
		Cost:        cost,
		Distance:    distance,
		RepeatIndex: msg.RepeatIndex,
	}

	match.Vars = make(map[string]Match)
	for k, v := range ctx.Vars {
		match.Vars[k] = v
	}

	return match
}

// CallComputeFunction calls method defined by name
// *name* must be defined as a function in *Models*
func CallComputeFunction(name string, ctx Context) (int64, error) {
	logger.Debugf("CallComputeFunction %s", name)
	fn, ok := Models[name]
	if ok {
		fnc := reflect.ValueOf(fn)
		in := make([]reflect.Value, 1)
		in[0] = reflect.ValueOf(ctx)
		values := fnc.Call(in)
		err := values[1].Interface()
		if err == nil {
			return values[0].Int(), nil
		}
		logger.Errorf("Failed to evaluate %s", name)
		return 0, fmt.Errorf("Failed to evaluate %s", name)

	}
	return 0, fmt.Errorf("could not find requested method %s", name)
}

// EvalComputeFunction generate code to evaluate a compte string (vars.R1.Len + 2 for example)
func EvalComputeFunction(compute string) string {
	if compute == "" {
		return "return 0, nil"
	}
	res := ""
	re := regexp.MustCompile(`vars\.(\w+).`)
	matches := re.FindAllSubmatch([]byte(compute), -1)
	if matches == nil {
		return "\nreturn int64(" + compute + "), nil\n"
	}
	for _, submatch := range matches {
		match := submatch[1]
		res += fmt.Sprintf(`
    if _, ok := ctx.Vars["%s"]; !ok {
      return 0, fmt.Errorf("variable not found")
    }
`, string(match))
		compute = strings.ReplaceAll(compute, "vars."+string(match), "ctx.Vars[\""+string(match)+"\"]")
	}
	res += "\nreturn int64(" + compute + "), nil\n"
	return res
}

// HasUndef checks if match has undefined variables
func HasUndef(m *Match) bool {
	if m.Defined {
		fmt.Printf("Defined, skip %+v\n", m)
		return false
	}
	hasUndef := false
	if len(m.Match) > 0 {
		for i := 0; i < len(m.Match); i++ {
			subm := &m.Match[i]
			subundef := HasUndef(subm)
			if subundef {
				hasUndef = true
				break
			}
		}
	}
	if hasUndef {
		m.PrevFoundMatch = m.Match
		m.Match = nil
	}
	return true
}

// GetUndef copies found matches to PrevFoundMatch for a new analysis run
func GetUndef(m *Match) {
	m.PrevFoundMatch = make([]Match, len(m.Match))
	for i := 0; i < len(m.Match); i++ {
		subm := m.Match[i]
		GetUndef(&subm)
		m.PrevFoundMatch[i] = subm
	}
	m.Match = make([]Match, 0)
}
