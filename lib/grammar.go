package logol

import (
	"bytes"
	"fmt"

	"text/template"

	yaml "gopkg.in/yaml.v2"
)

// Morphism defines mapping between characters
type Morphism struct {
	Morph map[string][]string
}

// ParamDef defines function inputs and outputs
type ParamDef struct {
	Comment string
	Inputs  []string
	Outputs []string
}

// VariableModel TODO
type VariableModel struct {
	Name    string
	Comment string
	Param   []string
}

// Repeat TODO
type Repeat struct {
	Min     string
	Max     string
	Spacer  RangeConstraint
	Overlap RangeConstraint
}

// RangeConstraint TODO
type RangeConstraint struct {
	Min string
	Max string
}

// StringConstraint TODO
type StringConstraint struct {
	Comment  string
	Content  string
	SaveAs   string `yaml:"saveas"`
	Size     RangeConstraint
	Start    RangeConstraint
	End      RangeConstraint
	Morphism string // name of morphism
	Reverse  bool
}

// StructConstraint TODO
type StructConstraint struct {
	Comment  string
	Cost     RangeConstraint // range [3,4]
	Distance RangeConstraint // range [10,20]
	Percent  string          // ac:80 80% of ac
}

// NegConstraint TODO
type NegConstraint struct {
	Value             string
	StringConstraints StringConstraint
	StructConstraints StructConstraint
}

// Variable TODO
type Variable struct {
	Comment             string
	Value               string
	Next                []string
	Model               VariableModel
	StringConstraints   StringConstraint `yaml:"string_constraints"`
	StructConstraints   StructConstraint `yaml:"struct_constraints"`
	NegativeConstraints []NegConstraint
	Overlap             bool
	OverlapConstraint   RangeConstraint
	Not                 bool
	Spacer              bool
	SpacerConstraint    RangeConstraint
	Repeat              Repeat
}

// Model TODO
type Model struct {
	Comment string
	Param   []string
	Meta    []string
	Start   []string
	Vars    map[string]Variable
}

// ModelDef TODO
type ModelDef struct {
	Model   string
	Param   []string
	Nomatch string // Id of the variable the model should not match
	Comment string
}

// Grammar defines the grammar used in search
type Grammar struct {
	Comment   string
	Options   map[string]int64
	Morphisms map[string]Morphism
	Models    map[string]Model
	Run       []ModelDef
	Meta      []string // Meta constraints on models e.g. #mod1 < # mod2
	Sequence  string
}

// LoadGrammar loads a grammar file content
func LoadGrammar(grammar []byte) (Grammar, error) {
	g := Grammar{}
	err := yaml.Unmarshal(grammar, &g)
	return g, err
}

// ModelInfo TODO
type ModelInfo struct {
	Name   string
	Params []string
	Code   string
	Desc   string
}

func getNextCalls(modelName string, varName string, model Model, varNames []string, isModel bool) string {

	var tmpl = `
	{{ $nbChans := len .VarNames}}
	{{ $modelName := .ModelName }}
	{{ $modelVarName := .VarName }}
{{range $index, $varName := .VarNames}}
	chin_{{$modelName}}_{{$varName}} := make(chan logol.Match)
	chout_{{$modelName}}_{{$varName}} := make(chan logol.Match)
	// call {{$modelName}}_{{$varName}}
	go {{$modelName}}_{{$varName}}(chin_{{$modelName}}_{{$varName}}, chout_{{$modelName}}_{{$varName}}, ctx)
{{end}}
	
	// Should search for pattern and send matches to chin_x_x
	nbChan := {{$nbChans}} + 1
	nbChanStop := 0
	/*
{{ if gt $nbChans 0 }}
	nbChan := {{$nbChans}}
	nbChanStop := 0
{{ end }}
	*/
	noMoreMatches := false
	{{if ne $modelName $modelVarName}}
	repeat := false
	{{end}}
	// Chans for repeat, will be used only if we have some repeats enabled
	chin_repeat := make(chan logol.Match)
	chout_repeat := make(chan logol.Match)
	chout_repeat_closed := false

	go func() {
		for true {
			msgIn, more := <-chin
			logger.Debugf("Message received at {{$modelName}}_{{$modelVarName}} %+v, more? %+v", msgIn, more)
			if more {
				if msgIn.RepeatIndex > 0 {
					ctx.IsRepeat = true
				}
				ctx.Progress <- "{{$modelName}}_{{$modelVarName}}:T"
				ctx.Position = msgIn.Position
				ctx.Vars = make(map[string]logol.Match)
				if msgIn.Spacer {
					ctx.Spacer = true
				} else {
					ctx.Spacer = false
				}
				for k,v := range msgIn.Vars {
					ctx.Vars[k] = v
				}
	{{if eq $modelName $modelVarName}}
		{{range $index, $aChan := .VarNames }}
				logger.Debugf("send to chin_{{$modelName}}_{{$aChan}} %+v", msgIn)
				chin_{{$modelName}}_{{$aChan}} <- msgIn
		{{end}}
		close(chout_repeat)
		chout_repeat_closed = true
		nbChanStop++
	{{else}}
				var prevMatch logol.Match
				prevMatches := msgIn.PrevFoundMatch
				hasPrevMatch := false
				if len(msgIn.PrevFoundMatch) > 0 {
					prevMatch = msgIn.PrevFoundMatch[0]
					prevMatches = prevMatches[1:]
					hasPrevMatch = true
				} else {
					if prevMatches == nil {
						prevMatches = make([]logol.Match, 0)
					}
				}
				fc := logol.NewSearchComponent(ctx, "{{$modelName}}", "{{$modelVarName}}", hasPrevMatch, prevMatch)
				go fc.Items()
				nbMatches := 0
				for m := range fc.Iterator() {
					if os.Getenv("LOGOL_SLOW") != "" {
						slow, errSlow := strconv.Atoi(os.Getenv("LOGOL_SLOW"))
						if errSlow == nil {
						time.Sleep(time.Duration(slow) * time.Second)
						}
					}
					if m.Defined {
						ctx.Progress <- "{{$modelName}}_{{$modelVarName}}:M"
					}
					if m.Pattern == "" {
						m.Pattern = "{{$modelName}}-{{$modelVarName}}"
					}
					m.RepeatIndex = msgIn.RepeatIndex

					newMsg := logol.Match{
						Position: m.Position + m.Len,
						Match:    append(msgIn.Match, m),
						Model: "{{$modelName}}",
						VarName: "{{$modelVarName}}",
						PrevFoundMatch: prevMatches,
					}

					newMsg.Vars = make(map[string]logol.Match)
					// for k, v := range fc.Context().Vars {
					for k, v := range msgIn.Vars {
						newMsg.Vars[k] = v
					}
					for k, v := range m.Vars {
						newMsg.Vars[k] = v
					}
					/*
					newMsg.Vars = ctx.Vars
					*/
					saveAs, save := fc.SaveAs()
					// TODO manage repeat for SaveAs
					if save {
						logger.Debugf("Save %s %+v", saveAs, m)
						newMsg.Vars[saveAs] = m
						if m.RepeatIndex > 0 && m.Defined {
							newMsg.Vars[saveAs] = logol.MergeRepeats(ctx, m, msgIn.Match[len(msgIn.Match) - int(m.RepeatIndex):])
						}
					}
					nbMatches++

					// Manage repeat case, if not yet started
					// Do we have repeats?
					methRepeatMinName := "{{$modelName}}_{{$modelVarName}}RepeatMin"
					methRepeatMaxName := "{{$modelName}}_{{$modelVarName}}RepeatMax"
					minRepeat, minRepeatErr := logol.CallComputeFunction(methRepeatMinName, ctx)
					maxRepeat, maxRepeatErr := logol.CallComputeFunction(methRepeatMaxName, ctx)
					if minRepeatErr != nil || maxRepeatErr != nil {
						newMsg.Defined = false
					} else {
						if minRepeat > 0 && maxRepeat == 0 {
							panic("MaxRepeat must be > 0")
						}
						if (minRepeat == 0 && maxRepeat == 0) || m.RepeatIndex >= uint64(maxRepeat) - 1{
							if !chout_repeat_closed {
								close(chout_repeat)
								chout_repeat_closed = true
								nbChanStop++
							}
						} else {
							if m.Defined {
								if !repeat {
									repeat = true
									// for repeat call ourselves {{$modelName}}_{{$modelVarName}}
									logger.Debugf("repeat, call myself")
									go {{$modelName}}_{{$modelVarName}}(chin_repeat, chout_repeat, ctx)
								}
								// Send back to myself
								repeatMsg := newMsg
								repeatMsg.RepeatIndex = msgIn.RepeatIndex + 1
								if !hasPrevMatch || (prevMatches[0].Model == "{{$modelName}}" && prevMatches[0].VarName == "{{$modelVarName}}") {
									logger.Debugf("send to chin_repeat %+v\n", newMsg) 
									chin_repeat <- repeatMsg
								}
								// If min not reached or max reached, skip other next components
								if newMsg.RepeatIndex < uint64(minRepeat) || newMsg.RepeatIndex > uint64(maxRepeat) {
									continue
								}
							}
						}
					}


		{{if eq $nbChans 0}}
					chout <- newMsg
		{{else}}
					if !m.Defined {
						newMsg.Spacer = true
					}
		{{end}}

		{{range $index, $aChan := .VarNames }}
					if !hasPrevMatch || (prevMatches[0].Model == "{{$modelName}}" && prevMatches[0].VarName == "{{$aChan}}") {
						logger.Debugf("send to chin_{{$modelName}}_{{$aChan}} %+v\n", newMsg)
						chin_{{$modelName}}_{{$aChan}} <- newMsg
					}
		{{end}}
				} // End of fc.Iterator
	{{end}}
			} else {
				logger.Debugf("Receive stop request")
				{{if eq $nbChans 0}}
					if ! repeat {
					logger.Debugf("send stop message back to out channel")
					close(chout)
					noMoreMatches = true
					} else {
						close(chin_repeat)
					}
				{{else}}
					{{range $index, $aChan := .VarNames}}
					logger.Debugf("send stop to chin_{{$modelName}}_{{$aChan}}\n")
					close(chin_{{$modelName}}_{{$aChan}})
					{{end}}
					close(chin_repeat)
				{{end}}
				{{if eq $modelName $modelVarName}}
				repeat := false
				{{end}}
					if !repeat && !chout_repeat_closed {
						close(chout_repeat)
						chout_repeat_closed = true
						nbChanStop++
					}
					break
			}			
			/*
			{{if eq $nbChans 0}}
				logger.Debugf("send stop message back to out channel")
				close(chout)
				noMoreMatches = true
			{{end}}
			{{range $index, $aChan := .VarNames}}
				logger.Debugf("send stop to chin_{{$modelName}}_{{$aChan}}\n")
				close(chin_{{$modelName}}_{{$aChan}})
			{{end}}
				close(chin_repeat)
				break
			*/
			
		}
	}()
{{range $index, $aChan := .VarNames}}
	chout_{{$modelName}}_{{$aChan}}_closed := false
{{end}}
	for ! noMoreMatches {
		/*
{{if eq $nbChans 0}}
		//logger.Debugf("wait in {{$modelName}}_{{$modelVarName}}})
		time.Sleep(1 * time.Second)
{{else}}
		select {
{{end}}
		*/
		select {
{{range $index, $aChan := .VarNames}}
			case msgOut, more := <-chout_{{$modelName}}_{{$aChan}}:
				if more {
					logger.Debugf("got msg from chout_{{$modelName}}_{{$aChan}}")
					chout <- msgOut
				} else {
					if ! chout_{{$modelName}}_{{$aChan}}_closed {
						nbChanStop++
						logger.Debugf("got close from chout_{{$modelName}}_{{$aChan}}")
						if nbChanStop == nbChan {
							noMoreMatches = true
							close(chout)
						}
						chout_{{$modelName}}_{{$aChan}}_closed = true
					}
				}
{{end}}
			case msgOut, more := <-chout_repeat:
				if more {
					logger.Debugf("got msg from chout_repeat")
					chout <- msgOut
				} else {
					if ! chout_repeat_closed {
						nbChanStop++
						logger.Debugf("got close from chout_repeat")
						if nbChanStop == nbChan {
							noMoreMatches = true
							close(chout)
						}
						chout_repeat_closed = true
					}
				}
		}
	}
/*
{{if gt $nbChans 0 }}
		}
{{end}}
*/
{{if eq $nbChans 0}}
	for ! noMoreMatches {
		time.Sleep(1 * time.Second)
	}
{{end}}
	logger.Debugf("Exit from {{$modelName}}_{{$modelVarName}}")
`
	type Data struct {
		ModelName string
		VarName   string
		VarNames  []string
	}
	tp, err := template.New("getNextCalls").Parse(tmpl)
	if err != nil {
		panic(err)
	}
	data := Data{
		ModelName: modelName,
		VarName:   varName,
		VarNames:  varNames,
	}
	var code bytes.Buffer
	err = tp.Execute(&code, data)
	if err != nil {
		panic(err)
	}

	return code.String()
}

// GenerateModel TODO
func GenerateModel(name string, model Model) (string, error) {
	data := `
// {{.Name}} var called by {{.Desc}}
func {{.Name}}(chin chan logol.Match, chout chan logol.Match, ctx logol.Context) error {
	logger.Debugf("Start model {{.Name}}\\n")



	{{.Code}}
	return nil
}
`
	varDef := ""
	modelCode := ""
	var writer bytes.Buffer

	evalFunc := `
{{range $key, $code := .Code}}
func {{$key}}(ctx logol.Context) (int64, error) {
	{{$code}}
}
{{end}}
`

	type evalFuncStruct struct {
		Name string
		Code map[string]string
	}
	tmpl, err := template.New("evalFun").Parse(evalFunc)
	evalData := evalFuncStruct{
		Name: name,
		Code: make(map[string]string),
	}
	for variableName, variableDef := range model.Vars {
		evalData.Code[name+"_"+variableName+"SizeMin"] = EvalComputeFunction(variableDef.StringConstraints.Size.Min)
		evalData.Code[name+"_"+variableName+"SizeMax"] = EvalComputeFunction(variableDef.StringConstraints.Size.Max)

		evalData.Code[name+"_"+variableName+"StartMin"] = EvalComputeFunction(variableDef.StringConstraints.Start.Min)
		evalData.Code[name+"_"+variableName+"StartMax"] = EvalComputeFunction(variableDef.StringConstraints.Start.Max)

		evalData.Code[name+"_"+variableName+"SpacerMin"] = EvalComputeFunction(variableDef.SpacerConstraint.Min)
		evalData.Code[name+"_"+variableName+"SpacerMax"] = EvalComputeFunction(variableDef.SpacerConstraint.Max)

		evalData.Code[name+"_"+variableName+"EndMin"] = EvalComputeFunction(variableDef.StringConstraints.End.Min)
		evalData.Code[name+"_"+variableName+"EndMax"] = EvalComputeFunction(variableDef.StringConstraints.End.Max)

		evalData.Code[name+"_"+variableName+"CostMin"] = EvalComputeFunction(variableDef.StructConstraints.Cost.Min)
		evalData.Code[name+"_"+variableName+"CostMax"] = EvalComputeFunction(variableDef.StructConstraints.Cost.Max)

		evalData.Code[name+"_"+variableName+"DistanceMin"] = EvalComputeFunction(variableDef.StructConstraints.Distance.Min)
		evalData.Code[name+"_"+variableName+"DistanceMax"] = EvalComputeFunction(variableDef.StructConstraints.Distance.Max)

		evalData.Code[name+"_"+variableName+"RepeatMin"] = EvalComputeFunction(variableDef.Repeat.Min)
		evalData.Code[name+"_"+variableName+"RepeatMax"] = EvalComputeFunction(variableDef.Repeat.Max)
		evalData.Code[name+"_"+variableName+"RepeatSpacerMin"] = EvalComputeFunction(variableDef.Repeat.Spacer.Min)
		evalData.Code[name+"_"+variableName+"RepeatSpacerMax"] = EvalComputeFunction(variableDef.Repeat.Spacer.Max)
		evalData.Code[name+"_"+variableName+"RepeatOverlapMin"] = EvalComputeFunction(variableDef.Repeat.Overlap.Min)
		evalData.Code[name+"_"+variableName+"RepeatOverlapMax"] = EvalComputeFunction(variableDef.Repeat.Overlap.Max)

		evalData.Code[name+"_"+variableName+"OverlapMin"] = EvalComputeFunction(variableDef.OverlapConstraint.Min)
		evalData.Code[name+"_"+variableName+"OverlapMax"] = EvalComputeFunction(variableDef.OverlapConstraint.Max)

	}

	err = tmpl.Execute(&writer, evalData)
	if err != nil {
		logger.Errorf("Error during template parsing %+v", err)
	}
	varDef += fmt.Sprint(writer.String())
	writer.Reset()

	for varName, varValue := range model.Vars {
		modelInfo := ModelInfo{
			Name:   fmt.Sprintf("%s_%s", name, varName),
			Params: make([]string, 0),
			Code:   getNextCalls(name, varName, model, varValue.Next, false), // Should set a call for each next var
			Desc:   model.Comment,
		}
		tmpl, err := template.New("model").Parse(data)
		if err != nil {
			return "", err
		}
		err = tmpl.Execute(&writer, modelInfo)
		varDef += fmt.Sprint(writer.String())
		writer.Reset()
	}
	for _, start := range model.Start {
		modelCode += fmt.Sprintf("// call %s_%s\n", name, start)
	}
	modelDef := ""
	/*
		params := make([]string, len(model.Param))
		for i, param := range model.Param {
			params[i] = param
		}
	*/
	modelInfo := ModelInfo{
		Name:   name,
		Params: model.Param,
		Code:   getNextCalls(name, name, model, model.Start, true),
	}

	tmpl, err = template.New("model").Parse(data)
	if err != nil {
		return "", err
	}
	err = tmpl.Execute(&writer, modelInfo)
	modelDef += fmt.Sprint(writer.String())
	writer.Reset()

	return modelDef + varDef, nil
}

// ModelCall TODO
type ModelCall struct {
	Fn     string
	Params []string
}

// Context TODO
type Context struct {
	Position uint64
	Spacer   bool
	// Match       *Match
	Vars        map[string]Match
	Progress    chan string
	Grammar     *Grammar
	Sequence    *Sequence
	SequenceLru *SequenceLru
	Kind        string // DNA or RNA or Protein, defaults DNA
	IsRepeat    bool   // Are we searching for repeats
}

// DoResult TODO
func DoResult(match Match) {
	logger.Errorf("Not yet implemented")
	logger.Infof("Match %+v", match)
}

// AndModels TODO
func AndModels(models []ModelCall) (string, string) {
	data := ""
	inputChan := "chin"
	outputChan := "chout"
	for _, model := range models {
		outputChan = model.Fn + "Chan"
		data += fmt.Sprintf(`
		%s := make(chan logol.Match)	
		go %s(%s, %s, ctx)
		`, outputChan, model.Fn, inputChan, outputChan)
		inputChan = outputChan
	}
	// data += "chout := " + outputChan + "\n"
	return data, outputChan
}
