package logol

import (
	"strings"
	"testing"
)

func TestEvalComputeFunction(t *testing.T) {
	test := ""
	testRes := "return 0, nil"
	res := EvalComputeFunction(test)
	if res != testRes {
		t.Errorf("should have %s, got %s", testRes, res)
	}
	test = "vars.R1.Len"
	res = EvalComputeFunction(test)
	if !strings.Contains(res, "ctx.Vars[\"R1\"]") {
		t.Errorf("code should refer to context var: got %s", res)
	}

	test = "vars.R1.Len + vars.R2.Len"
	res = EvalComputeFunction(test)
	if !strings.Contains(res, "ctx.Vars[\"R1\"]") || !strings.Contains(res, "ctx.Vars[\"R2\"]") {
		t.Errorf("code should refer to context var: got %s", res)
	}

}
