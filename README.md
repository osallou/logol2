# Logol 2

rewrite of logol in Go language

## Usage

    go build && ./logol2 -h

## Test

    go build && ./logol2 -grammar testdata/simple.yml -output testdata/run
    cd testdata
    LOGOL_DEBUG=1 go run run.go  sequence.txt

## Web UI

It is possible to launch generated code with an UI to check for model

    LOGOL_WEB=1 LOGOL_LISTEN=:8080 go run run.go

Then go to [http://localhost:8080/model](http://localhost:8080/model)

LOGOL_WEB=X will "block" the process until Ctrl-C

During search, if LOGOL_LISTEN is set, all urls are available:

* /model : graphical view of the model
* /progress: JSON stats on number of matches in each component

## Env variables

* LOGOL_DEBUG = 1 activate debug level logging
* LOGOL_SLOW = X pause X seconds between each search

## TODO

* Use zap log library and add model/var as params in logs
* Cost & distance
* morphism (with reverse) [done, to be tested]
* overlap [done, to be tested]
* test spacers (specially after unknown) [done, to be tested]
* spacers and overlap in repeats [done, to be tested]
* manage case repeat 0,X (zero repeat allowed, we have no min but we have a max)
* can we enhance search using known constraints
* use cassiopee lib for exact/approximate search with spacer
