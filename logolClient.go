package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"path"
	"strings"

	"github.com/alecthomas/template"
	logol "github.com/osallou/logol2/lib"
)

var logger = logol.GetLogger("logol.client")

// Version current version of software
var Version string

// Buildtime time of the build
var Buildtime string

func main() {
	outFile := flag.String("output", "", "Output file (default: same as grammar file")
	grammarFile := flag.String("grammar", "", "Path to grammar yml file")
	version := flag.Bool("version", false, "show version and build info")
	flag.Parse()

	if *version {
		fmt.Printf("Version: %s, Build time: %s\n", Version, Buildtime)
		return
	}

	if *grammarFile == "" {
		fmt.Println("Missing grammar file in command line")
		return
	}

	outputPath := strings.TrimSuffix(*grammarFile, path.Ext(*grammarFile)) + ".go"
	if *outFile != "" {
		outputPath = *outFile + ".go"
	}

	grammarpath := *grammarFile
	g, gerr := ioutil.ReadFile(grammarpath)
	if gerr != nil {
		panic("Cannot read grammar file")
	}
	grammar, err := logol.LoadGrammar([]byte(g))
	if err != nil {
		logger.Errorf("Failed to load grammar %+v", err)
		return
	}

	grammarJSON, grammarJSONErr := json.Marshal(grammar)
	if grammarJSONErr != nil {
		panic("Failed to decode grammar")
	}

	data := `package main
import (
	"encoding/json"
	"fmt"
	// "io/ioutil"
	"os"
	"strconv"
	"time"
	logol "github.com/osallou/logol2/lib"
)

var logger = logol.GetLogger("logol.client")
	`
	for modelName, modelData := range grammar.Models {
		modelTemplate, err := logol.GenerateModel(modelName, modelData)
		if err != nil {
			logger.Errorf("Failed to generate code %+v", err)
			return
		}
		data += modelTemplate + "\n"

	}

	type Node struct {
		ID    string `json:"id"`
		Label string `json:"label"`
		Size  int    `json:"size"`
		X     int    `json:"x"`
		Y     int    `json:"y"`
	}
	type Edge struct {
		ID     string `json:"id"`
		Source string `json:"source"`
		Target string `json:"target"`
		Type   string `json:"type"`
	}
	type Graph struct {
		Nodes []Node `json:"nodes"`
		Edges []Edge `json:"edges"`
	}
	graph := Graph{
		Nodes: make([]Node, 0),
		Edges: make([]Edge, 0),
	}
	modY := 10
	modX := 10
	varX := 20
	for modName, modInfo := range grammar.Models {
		info := ""
		if modInfo.Comment != "" {
			info = fmt.Sprintf("(%s)", modInfo.Comment)
		}
		graph.Nodes = append(graph.Nodes, Node{
			ID:    modName,
			Label: modName + info,
			X:     modX,
			Y:     modY,
			Size:  1,
		})
		modY += 10
		for _, next := range modInfo.Start {
			graph.Edges = append(graph.Edges, Edge{
				ID:     modName + "_" + next,
				Source: modName,
				Target: modName + "-" + next,
				Type:   "arrow",
			})
		}
		for varName, varInfo := range modInfo.Vars {
			extra := ""
			if varInfo.Model.Name != "" {
				extra = fmt.Sprintf("[->%s]", varInfo.Model.Name)
			}
			info := ""
			if varInfo.Comment != "" {
				info = fmt.Sprintf("(%s)", varInfo.Comment)
			}
			graph.Nodes = append(graph.Nodes, Node{
				ID:    modName + "-" + varName,
				Label: modName + "-" + varName + info + extra,
				X:     varX,
				Y:     modY,
				Size:  1,
			})
			varX += 10
			for _, next := range varInfo.Next {
				graph.Edges = append(graph.Edges, Edge{
					ID:     modName + "_" + next,
					Source: modName + "-" + varName,
					Target: modName + "-" + next,
					Type:   "arrow",
				})
			}

		}
	}

	data += `func main() {`

	graphJSON, _ := json.Marshal(graph)
	data += fmt.Sprintf("\n    graphJSON := `%s`\n", graphJSON)
	data += fmt.Sprintf("\n    grammarJSON := `%s`\n", grammarJSON)
	data += `
	if os.Getenv("LOGOL_WEB") != "" {
		logol.ServeProgress(graphJSON)
		return
	}
	fmt.Printf("Search in sequence %s\n", os.Args[1])

	go logol.ServeProgress(graphJSON)
	if len(os.Args) < 2 {
		panic("No sequence file given as input")
	}

	f, errf := os.Create(os.Args[1]+".logol")
	if errf != nil {
		panic("Cannot write to result file")
	}
	defer f.Close()
	//grammarpath := os.Args[1]
	//g, _ := ioutil.ReadFile(grammarpath)
	//grammar, err := logol.LoadGrammar([]byte(g))
	var grammar logol.Grammar
	err := json.Unmarshal([]byte(grammarJSON), &grammar)
	if err != nil {
		logger.Errorf("Failed to load grammar %+v", err)
		return
	}
	sequence := logol.NewSequence(os.Args[1])
	lru := logol.NewSequenceLru(sequence)
	ctx := logol.Context {
		Position: 0,
		Spacer: false,
		// Match: nil,
		Progress: make(chan string),
		Grammar: &grammar,
		Sequence: &sequence,
		SequenceLru: &lru,
	}
	go logol.TrackProgress(ctx.Progress)

`
	modelsCall := make([]logol.ModelCall, 0)
	mainParams := make(map[string]bool)
	for _, run := range grammar.Run {
		for _, param := range run.Param {
			if _, ok := mainParams[param]; !ok {
				//data += fmt.Sprintf("    var %s *logol.Variable\n", param)
				mainParams[param] = true
			}
		}
		modelsCall = append(modelsCall, logol.ModelCall{
			Fn:     run.Model,
			Params: run.Param,
		})

	}

	data += "    logol.Models = make(map[string]interface{})\n"

	//look over model/vars to add functions in logol.models on SizeMin, SizeMax, etc.
	funcs := []string{"SizeMin", "SizeMax", "StartMin", "StartMax", "EndMin", "EndMax", "SpacerMin", "SpacerMax", "CostMin", "CostMax", "DistanceMin", "DistanceMax", "RepeatMin", "RepeatMax", "RepeatSpacerMin", "RepeatSpacerMax", "RepeatOverlapMin", "RepeatOverlapMax", "OverlapMin", "OverlapMax"}
	for modelName, model := range grammar.Models {
		for variable := range model.Vars {
			for _, fn := range funcs {
				funcName := modelName + "_" + variable + fn
				data += fmt.Sprintf(`
	logol.Models["%s"]=%s
			`, funcName, funcName)
			}
		}
	}

	for modelName := range grammar.Models {
		data += fmt.Sprintf(`
	logol.Models["%s"] = %s		
`, modelName, modelName)
	}

	search := `
	{{ $nbModels := .NbModels }}
	{{ $lastModelIndex := .LastModelIndex }}
	matchStart := logol.Match{
		Value:  "init",
		Match:  make([]logol.Match, 0),
		Vars:   make(map[string]logol.Match),
		Spacer: true,
	}
	matchesToEvaluate := make([]logol.Match, 1)
	matchesToEvaluate[0] = matchStart
	totalMatches := 0
	for len(matchesToEvaluate) > 0 {
		matchStart = matchesToEvaluate[0]
		matchesToEvaluate = matchesToEvaluate[1:]
		// Do search
		chin := make(chan logol.Match)
		{{ $inputChan := "chin" }}
		{{ $outputChan := "chout"}}
	{{range $index, $model := .Models}}
		{{ $model.Fn }}Chan := make(chan logol.Match)
	{{ if eq $index 0 }}
		go {{ $model.Fn }}(chin, {{ $model.Fn }}Chan, ctx)
		{{ else }}
		go {{ $model.Fn }}({{ (index .Models $lastModelIndex).Fn}}Chan, {{ $model }}Chan, ctx)
		{{ end }}
	{{end}}
		
		chin <- matchStart
		close(chin)
		chout := {{ (index .Models $lastModelIndex).Fn }}Chan

		for m := range chout {
			newCtx := ctx
			newCtx.Vars = m.Vars
			result := logol.MergeMatches(m, newCtx, logol.Variable{})
			if result.Defined {
				resJSON, _ := json.Marshal(result)
				logger.Infof("got result: %+s", string(resJSON))
				totalMatches++
				f.Write(resJSON)
				f.WriteString("\n")
			} else {
				logol.GetUndef(&result)
				if ! result.PrevFoundMatch[0].Defined {
					result.Position = 0
					result.Spacer = true
				} else {
					result.Spacer = false
				}
				matchesToEvaluate = append(matchesToEvaluate, result)
				resJSON, _ := json.Marshal(result)
				logger.Debugf("undefined %+s", string(resJSON))
			}

		}
		nbUndef := len(matchesToEvaluate)
		if nbUndef > 0 {
			logger.Debugf("Undefined matches to reevaluate: %d", nbUndef)
		}
	}


	stats, err := json.Marshal(logol.GetProgress())
	if err == nil {
		logger.Debugf("Stats: %+s", stats)
	}
	logger.Infof("Number of match: %d", totalMatches)


`
	searchTemplate, err := template.New("search").Parse(search)
	if err != nil {
		panic(err)
	}

	type SearchData struct {
		Models         []logol.ModelCall
		NbModels       int
		LastModelIndex int
	}
	searchData := SearchData{
		Models:         modelsCall,
		NbModels:       len(modelsCall),
		LastModelIndex: len(modelsCall) - 1,
	}
	var searchCode bytes.Buffer
	err = searchTemplate.Execute(&searchCode, searchData)
	data += searchCode.String()

	/*
			outStr, outChan := logol.AndModels(modelsCall)
			data += outStr
			data += `
			matchStart := logol.Match{
				Value: "init",
				Match: make([]logol.Match, 0),
				Vars: make(map[string]logol.Match),
				Spacer: true,
			}
			chin <- matchStart

			`
			data += "chout := " + outChan
			data += `
			over := false
			totalMatches := 0

			go func() {
				for m := range chout {
					newCtx := ctx
					newCtx.Vars = m.Vars
					result := logol.MergeMatches(m, newCtx, logol.Variable{})
					//TODO see logol.SearchForUndefined

					resJson, _ := json.Marshal(result)
					logger.Infof("got result: %+s", string(resJson))
					totalMatches++
				}
				over = true
			}()
			close(chin)

			for !over {
				time.Sleep(3 * time.Second)
			}
			stats, err := json.Marshal(logol.GetProgress())
			if err == nil {
				logger.Debugf("Stats: %+s", stats)
			}
			logger.Infof("Number of match: %d", totalMatches)
		`

			data += "}\n"
	*/
	data += "}\n"

	// outputPath := filepath.Join("testdata", "run.go")
	errWrite := ioutil.WriteFile(outputPath, []byte(data), 0644)
	if errWrite != nil {
		logger.Errorf("Failed to write grammar file %+v", errWrite)
		return
	}

}
